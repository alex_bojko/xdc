from django.template import Context, Template
from django.http import HttpResponse, HttpResponseRedirect
from profiles.models import Profile
from django.views.generic.simple import direct_to_template
from django.views.decorators.csrf import csrf_exempt
from .models import Subscribe
from django.utils import simplejson


def subscribe_user(request):
    if request.user.is_authenticated():
        m = Subscribe.objects.create(subscribed_user = request.user, subscribe_type = 1)
        return HttpResponseRedirect('/datingCenter')   
    else:
        return HttpResponseRedirect('/datingCenter')      


@csrf_exempt
def subscribe(request, template="subscribe/subscribe.html"):
    if request.user.is_authenticated():
        if not Subscribe.objects.filter(subscribed_user = request.user):
            context = {'cur_prof_name' : request.user.username,}
            return direct_to_template(request, template, context)
        else: 
            return HttpResponseRedirect('/datingCenter')
    else:
        return HttpResponseRedirect('/datingCenter')

def check_payment(request):
    print request.user.username
    if Subscribe.objects.filter(subscribed_user = request.user):
        mes = '1'
        print '1'
    else:
        mes = '2'
        print '2'
    #ret = simplejson.loads(mes)
    return HttpResponse(mes)
