from django.db import models
from profiles.models import Profile

class Subscribe(models.Model):
    TYPE_CHOICES = (
        (0, 'No'),
        (1, 'opt1'),
        (2, 'opt2'),
    )
    subscribed_user = models.ForeignKey(Profile, blank=True, null=True)
    subscribe_type = models.IntegerField(choices=TYPE_CHOICES, default=0)
  #  start_date = models.DateTimeField(auto_now_add = True)
    
    
