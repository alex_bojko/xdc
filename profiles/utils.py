import time


def datetime_to_epoch(date):
    return time.mktime(date.timetuple())

