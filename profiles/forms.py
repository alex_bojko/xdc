from django import forms
from sorl.thumbnail import get_thumbnail
from django.conf import settings
from .models import Profile, Image
from django.forms.extras import widgets
import datetime
from captcha.fields import CaptchaField
import autocomplete_light
from messages.views import compose
from messages.forms import ComposeForm
from messages.fields import CommaSeparatedUserField
from django.forms.widgets import HiddenInput
import hashlib
from .utils import datetime_to_epoch
now = datetime.datetime.now().year
YEARS_RANGE = range(now-80, now-10)


class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['birth_date'].widget = widgets.SelectDateWidget(years=YEARS_RANGE)

    class Meta:
        model = Profile
        fields = ('about_me', 'looking_for', 'height', 'race', 'religion',
                  'hair_color', 'eye_color', 'body_type', 'profession',
                  'birth_date',  )

class ProfileSettingsForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('email_notifications',
                  'email_profile_views', 'third_party_email')

class ChangeEmailForm(forms.Form):
    def __init__(self, user, *args, **kwargs):
        self.user = user
        self.base_fields['email'].initial = self.user.email
        super(ChangeEmailForm, self).__init__(*args, **kwargs)

    email = forms.EmailField()

    def clean_email(self):
        email = self.cleaned_data['email']
        UserClass = self.user.__class__
        print "CLEAN EMAIL"
        if self.user.email == email: #self.cleaned_data['email']:
            return email
        if UserClass.objects.filter(email=email).exclude(email=self.user.email):
            raise forms.ValidationError("This email already exists!")
        return email

    def save(self, commit=True):
        self.user.email = self.cleaned_data['email']
        if commit:
            self.user.save()
        return self.user


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ('src',)

class LocationForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('location',)
        widgets = {'location': autocomplete_light.ChoiceWidget("CityAutocomplete")}

class SendMessageForm(ComposeForm):
    recipient = CommaSeparatedUserField(widget = HiddenInput())
    subject = forms.CharField( max_length=120, widget = HiddenInput())
    body = forms.CharField(label = '', widget=forms.Textarea(attrs={'style':'resize: none; overflow-y: hidden; position: absolute; height: 35px; width: 528px; line-height: normal; text-decoration: none; letter-spacing: normal;'}))

    def get_hash(self, sender):
        hsh = hashlib.md5(unicode(sender.id))
        hsh.update(unicode(int(datetime_to_epoch(datetime.datetime.now()))))
        return hsh.hexdigest()

    def save(self, *args, **kwargs):
        self.cleaned_data['subject'] = self.get_hash(kwargs.get('sender'))
        super(SendMessageForm, self).save(*args, **kwargs)


