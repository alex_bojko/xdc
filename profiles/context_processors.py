from django.conf import settings as dj_settings
from django.contrib.auth.forms import AuthenticationForm
def settings(request):
    return {"django_settings": dj_settings,
            "auth_form": AuthenticationForm()}
