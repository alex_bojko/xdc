from django.conf import settings
from django import template
import os 
register = template.Library()
from profiles.models import Profile, Image
#img = os.path.join(settings.STATIC_ROOT, "no_photo_safe_explicit_woman_100.png")

def if_safe(user, viewer):
    try:
        safe_activated = viewer.safe_mode
        im_valid = user.get_avatar().is_safe
        if not im_valid and safe_activated:
            if viewer.looking_for:
                sx = 'woman'
            else:
                sx = 'man'
            img = "no_photo_safe_explicit_" + sx + "_100.png"
            return settings.STATIC_URL[1:] + img
        return user.get_avatar().src
    except:
        pass

register.filter('if_safe', if_safe)

