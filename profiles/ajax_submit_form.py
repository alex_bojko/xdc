from django.utils.functional import Promise
from django.utils.encoding import force_unicode
from django.utils.simplejson import JSONEncoder
from django.utils import simplejson
from django import forms
from django.http import HttpResponse
from profiles.decorators import need_ajax
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt, csrf_protect

class LazyEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_unicode(obj)
        return obj

@need_ajax
def submit_form(request, *args, **kwargs):
    form_class = kwargs.pop('form_class')
    complete_func = kwargs.pop('complete_func', None)
    extra_args_func = kwargs.pop('callback', lambda request, *args, **kwargs: {})
    kwargs_form_data = extra_args_func(request, *args, **kwargs)
    kwargs_form_data['data'] = request.POST
    form = form_class(**kwargs_form_data)
    if form.is_valid():
        result_data = {
            'saved': True,
        }
        if complete_func:
            complete_func(request,result_data,kwargs_form_data,*args,**kwargs)
    else:
        if request.POST.getlist('fields'):
            fields = request.POST.getlist('fields') + ['__all__']
            errors = dict([(key, val) for key, val in form.errors.iteritems() if key in fields])
        else:
            errors = form.errors
        final_errors = {}
        for key, val in errors.iteritems():
            try:
                if not isinstance(form.fields[key], forms.FileField):
                    html_id = form.fields[key].widget.attrs.get('id') or form[key].auto_id
                    html_id = form.fields[key].widget.id_for_label(html_id)
                    final_errors[html_id] = val
            except:
                final_errors[key] = val
                break
        result_data = {
            'saved': False,
            'errors': final_errors,
        }
    json_serializer = LazyEncoder()
    return HttpResponse(json_serializer.encode(result_data), mimetype='application/json')
submit_form = require_POST(submit_form)
