from django.template import Context, Template
from django.views.generic.simple import direct_to_template
from django.contrib.auth import login
from django.shortcuts import get_object_or_404
from django.http import (HttpResponse, HttpResponseRedirect)
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import authenticate
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from helpdata.models import HelpData
import sorl
from .forms import (PhotoForm, ProfileForm, ProfileSettingsForm,
                    ChangeEmailForm, LocationForm, SendMessageForm
                   )
from locations.forms import SearchForm, XCityForm
from xcp_registration.forms import (RegistrationFormStep1, RegistrationFormStep2,
                                    RegistrationFormStep3)

from .models import Profile, DEFAULT_ABOUT_ME, Image,Update
from user_statistic.models import Viewed
#from locations.models import XCity
from cities_light.models import City
from .decorators import need_ajax
from endless_pagination.decorators import page_template
from locations.models import get_all_in_radius, filter_users
from django.contrib import messages
from messages.models import Message
from messages.views import compose
from subscribe.models import Subscribe
import datetime
from xcp_registration.forms import now
from user_statistic.models import Like

def index(request):
    context = {"reg_form": RegistrationFormStep1(initial = {'birth_date':datetime.date(now -27 ,1,1), 'tos':True})}
    users = []
    locs = get_all_in_radius(
        request.META.get('GEOIP_LATITUDE', 0),
        request.META.get('GEOIP_LONGTITUDE', 0))
    us = Profile.objects.filter(
        location__id__in=locs
    ).order_by('last_login')[:8]
    for i in range(9):
        try:
            users.append(us[i])
        except IndexError:
            users.append(None)
    context.update({'users': users})
    queryset = Profile.objects.filter(last_active__gt = datetime.datetime.now() - datetime.timedelta(minutes = 15)
                                     ).exclude(username = request.user.username)
    cur_list = []
    for i in range(9):
        try:
            cur_list.append(queryset[i])
        except:
            cur_list.append(None)
    context.update({'online_users':cur_list})
    if request.user.is_authenticated() and isinstance(request.user, Profile):
        if request.session.get('registration_step', 0) == 1:
            context.update({"reg_form2": RegistrationFormStep2(request.user)})
            return direct_to_template(request, 'profiles/index.html', context)
        elif request.session.get('registration_step', 0) == 2:
            context.update({"photo_form": PhotoForm(),
                            "reg_form3": RegistrationFormStep3()})
            return direct_to_template(request, 'profiles/index.html', context)
        else:
            return HttpResponseRedirect("/datingCenter/")
    else:
        return direct_to_template(request, 'profiles/index.html', context)

def if_subscribed(f):
    def wrapper(request):
        if Subscribe.objects.filter(subscribed_user = request.user):
            return f(request)
        else:
            return HttpResponseRedirect('/subscribe/')
    return wrapper

@login_required
def edit_profile(request):
    if request.method == 'POST':
        return direct_to_template(
            request, "profiles/profile_edit.html", {})
    settings_form = ProfileSettingsForm(instance=request.user)
    pwd_form = PasswordChangeForm(request.user)
    email_form = ChangeEmailForm(request.user)
    profile_form = ProfileForm(instance=request.user)

    return direct_to_template(
        request, 'profiles/profile_edit.html',
            {'image_form':PhotoForm,
             'profile':request.user,
             'settings_form': settings_form,
             'password_change_form': pwd_form,
             'change_email_form': email_form,
             'profile_form': profile_form,
             'location_form': LocationForm(),
             'owner': True
            }
    )

#@login_required
def view_profile(request, slug):
    profile = get_object_or_404(Profile, slug=slug)
    if request.user.is_authenticated():
        cur_user = request.user
        created, viewed = Viewed.objects.get_or_create(
            user=request.user, viewed=profile)
        try:
            Like.objects.get(user = request.user, like = profile)
            liked_or_not = True
        except:
            liked_or_not = False
    else:
        cur_user = None
        liked_or_not = None

    form = SendMessageForm(initial={'subject': 'message', 'recipient' : profile})
    return direct_to_template(
        request, 'profiles/profile_view.html',
        {'profile': profile,
         'form': form,
         'owner': False,
         'cur_user':cur_user,
         'liked_or_not':liked_or_not
        }
    )



@login_required
def save_profile_settings(request, result_data, kwargs_form_data, *args, **kwargs):
    # require password to change any setting
    mail_form = ChangeEmailForm(request.user, request.POST)
    if mail_form.is_valid():
        prof = mail_form.save()
    else:
        p_form = ProfileSettingsForm(instance=request.user, **kwargs_form_data)
    if p_form.is_valid():
        prof = p_form.save(commit=False)
        try:
            prof.save()
        except Exception as e:
            result_data['valid'] = False
            result_data['error'] = e
    return result_data

@login_required
def save_profile(request, result_data, kwargs_form_data, *args, **kwargs):
    form = ProfileForm(instance=request.user, **kwargs_form_data)
    if form.is_valid():
        prof = form.save()#commit=False)
        try:
            prof.save()
        except Exception as e:
            result_data['valid'] = False
            result_data['error'] = e
    return result_data


@login_required
def upload_photo(request):
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            img = form.save(commit=False)
            img.user = request.user
            img.save()
        #return direct_to_template(request, 'profiles/profile_edit.html',
                                  #{'image_form':form})
    return redirect('edit_profile')

@login_required
def change_safe_mode(request, redirect_url_name=''):
    print type(request.user)
    request.user.safe_mode = not request.user.safe_mode
    request.user.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required
def set_primary_avatar(request, slug):
    ava = request.user.image_set.filter(slug=slug, need_moderation=False).update(is_avatar=True)
    # notify user
    # create Update
    return HttpResponse('{"response":true}') if ava else HttpResponse('{"response":false}')


@login_required
def delete_update(request, slug):
    try:
        up = Update.objects.get(slug = slug, user = request.user)
        up.delete()
        print "True"
        return HttpResponse(simplejson.dumps({'response':True, 'error':False}))
    except:
        print "False"
        return HttpResponse(simplejson.dumps({'response':False, 'error':True}))

@login_required
def delete_message(request, s_id):
    print s_id
    try :
        Message.objects.get(subject = s_id, recipient__username = request.user.username).delete()
        return HttpResponse(simplejson.dumps({'response':True}))
    except:
        return HttpResponse(simplejson.dumps({'response':False}))

def return_form_html(request, render_as="table"):
    if request.is_ajax():
        render_choices = {'table': '<table>{}</table>',
                          'li': '<ul>{}</ul>',
                          'p': '{}'}
        if render_as not in render_choices:
            render_as = render_choices.keys()[0]
        form = RegistrationFormStep2(request.user)
        html = render_choices[render_as].format(
            getattr(form, 'as_'+render_as,
                    lambda *a, **k: 'Something went wrong. Please reload page.')()
        )
        return HttpResponse(html)
    else:
        return redirect('index')

@login_required
@need_ajax
def change_location(request):
    if request.method=='POST':
        form = LocationForm(request.POST, instance=request.user)
        if form.is_valid():
            print "VALID"
            form.save()
            return simplejson.dumps({'response': True})
        return simplejson.dumps({'response': False})

@page_template('profiles/search_page.html')
def search(request, template='profiles/search.html', extra_context=None):
    form = SearchForm(user=request.user)
    populate_users = lambda request: filter_users(
        lat=(request.META['GEOIP_LATITUDE']
             if request.user.is_anonymous() else
             request.user.location.latitude),
        lng=(request.META['GEOIP_LONGTITUDE'] if request.user.is_anonymous()
             else request.user.location.longitude)
    )
    if request.method == 'POST':
        # do search
        form = SearchForm(request.POST, request.user)
        if form.is_valid():
            users = form.do_search(for_user=request.user)
        else:
            users = populate_users(request)
    else:
        init = {}
        if request.user.is_authenticated():
            init['city'] = request.user.location
        else:
            init['city'] = get_all_in_radius(
                lat=request.META['GEOIP_LATITUDE'],
                lng=request.META['GEOIP_LONGTITUDE']
            )[0]

        print init
        form = SearchForm(
            user=request.user,
            initial=init
        )
        '''
        GENDER =
            0 - Man
            1 - Wooman
        '''
        users = populate_users(request).filter(looking_for=0, gender=1)

        #if form.is_valid():
            #print "\t\t\tform is valid"
        #else:
            #print form.errors
        #users = form.do_search(for_user=request.user)
    context = {
        'form': form,
        'users': users,
        'num': 20,
    }
    if extra_context is not None:
        context.update(extra_context)
    return direct_to_template(request, template, context)

@page_template('')
def online_users(request, template="profles/online.html", extra_context=None):
    '''settings should contain DEFAULT_ONLINE_TIME_SEC'''
    now = datetime.datetime.now()
    online_time = getattr(settings, "DEFAULT_ONLINE_TIME_SEC", 5*60)
    onl_d = now.replace(second=(now-datetime.timedelta(seconds=online_time)).second)
    context = {
        'online': Profile.objects.filter(last_login__gte=onl_d),
        'num': 9
    }
    if extra_context is not None:
        context.update(extra_context)
    return direct_to_template(request, template, context)
@if_subscribed
def send_message(request):
    return compose(request, recipient=None, form_class=SendMessageForm,
        template_name='profiles/send_private_message.html', success_url = '/datingCenter/')

'''
    if request.method == 'POST':
        form = SendMessageForm(request.POST)
        if form.is_valid():
            msg = Message(
                sender = sender,
                recipient = r,
                subject = subject,
                body = body,
            )
            msg.save()
            return HttpResponseRedirect('')
    else:
        form = SendMessageForm()

    return direct_to_template(
        request, 'profiles/profile_view.html',
        {'profile': profile, 'form': form, 'owner': False}
    )'''

@csrf_exempt
def helpdata(request):
    if request.method == 'POST':
        #print request.POST
        cur_data_id = request.POST['s']
        #print cur_data_id#, cur_data_id.get('s')
        q = HelpData.objects.filter(data_id = cur_data_id)[0]
       # print q.data
        return HttpResponse(q.data)

def datingcenter(request, template = 'datingCenter/datingCenter.html'):
    if request.user.is_authenticated():
        context = {}
        return direct_to_template(request, template, context)
    else:
        return HttpResponseRedirect('/')

def when(time):
    delta = datetime.datetime.now() - time
    res_dict = {
        'months':delta.days/30,
        'weeks':delta.days/7,
        'days':delta.days,
        'hours':delta.seconds/60/60,
        'minutes':delta.seconds/60,
        'seconds':delta.seconds
        }
    if res_dict.get('months'):
        return "%s %s ago" % (str(res_dict.get('months')), 'months')
    if res_dict.get('weeks'):
        return "%s %s ago" % (str(res_dict.get('weeks')), 'weeks')
    if res_dict.get('days'):
        return "%s %s ago" % (str(res_dict.get('days')), 'days')
    if res_dict.get('hours'):
        return "%s %s ago" % (str(res_dict.get('hours')), 'hours')
    if res_dict.get('minutes'):
        return "%s %s ago" % (str(res_dict.get('minutes')), 'minutes')
    if res_dict.get('seconds'):
        return "%s %s ago" % (str(res_dict.get('seconds')), 'seconds')


def get_inf(user):
    ''' usr = Profile.objects.filter(username = user)[0]
    if usr.gender == 1:
        sx = "W"
    else:
        sx = "M"
    age = (datetime.datetime.now().date - usr.birth_date).years
    if usr.location:
        location = usr.location.name
    else:
        location = ''
    s = "%s %d %s" % (sx, age, location)
    print s'''
    return 'fix'



def getMessageData(request):
    if request.user.is_authenticated():
        cur_message = Message.objects.filter(recipient = request.user)[:20]
        res_list = []
        for item in cur_message:
            cur_username = item.sender.username
            cur_profile = Profile.objects.filter(username = item.sender.username)[0]
            m = Image.objects.filter(user = cur_profile)
            if m:
                cur_image = m[0]
            else:
                cur_image = ''
            cur_dict = {
                'sender':cur_username,
                'body':item.body,
                'time':when(item.sent_at),
                'user_info':get_inf(cur_username),
                'img':cur_image,
                'link':str(cur_profile.slug),
                'message_id':item.subject,
                }
            res_list.append(cur_dict)

        return direct_to_template(request, 'message_list.html',{'res_list':res_list})
    else:
        return direct_to_template(request, 'message_list.html',{'res_list':[]})



def getUpdateData(request):
    if request.user.is_authenticated():
        cur_update = Update.objects.all().order_by('event_date')[:20]
        res_list = []
        for item in cur_update.reverse():
            cur_user = item.user
            m = Image.objects.filter(user = cur_user)
            if m:
                cur_image = m[0]
            else:
                cur_image = ''
            cur_dict = {
                'user':cur_user.username,
                'link':str(cur_user.slug),
                'img':cur_image,
                'text':item.message,
                'time':when(item.event_date),
                 }
       # print 'dict passsed'
            res_list.append(cur_dict)
        return direct_to_template(request, 'update_list.html',{'res_list':res_list})
    else:
        return direct_to_template(request, 'update_list.html',{'res_list':[]})

@csrf_exempt
def post_update(request):
    if request.method == 'POST':
        m = Update.objects.create(user = request.user, message = request.POST['update'],
                             event_type = 0)
        cur_user = m.user
        res_list = []
        n = Image.objects.filter(user = cur_user)
        if n:
            cur_image = n[0]
        else:
            cur_image = ''
        cur_dict = {
                'user':cur_user.username,
                'link':str(cur_user.slug),
                'img':cur_image,
                'text':m.message,
                'time':when(m.event_date),
                 }
        res_list.append(cur_dict)
        return direct_to_template(request, 'update_li.html', {'res_list':res_list})
    return HttpResponse(simplejson.dumps({'response':False}))


def set_img(request):
    if Image.objects.get(slug = request.GET['hash']):
        img = Image.objects.get(slug = request.GET['hash'])
        print img
        m = Image.objects.filter(user = img.user, is_avatar = True)
        print m
        if m:
            for i in m:
                i.is_avatar = False
                i.save()
                print 'ok'
        img.is_avatar = True
        img.save()
        spl = img.src.path
        im = sorl.thumbnail.get_thumbnail(spl, '320x310').url
        reload(sorl.thumbnail)
        print im
        return HttpResponse(simplejson.dumps({'response':True, 'src':im}))
    else:
        return HttpResponse(simplejson.dumps({'response':False}))


def del_img(request):
    try:
        img = Image.objects.get(slug = request.GET['hash']).delete()
        return HttpResponse(simplejson.dumps({'request':True}))
    except:
        return HttpResponse(simplejson.dumps({'request':False}))


def get_more(request):
    if request.user.is_authenticated():
        item = request.GET['item']
        print item
        cur_items = request.GET['cur_items']
        if item == 'update':
            cur_update = Update.objects.all().order_by('event_date')[cur_items:]
            print 1
            res_list = []
            for item in cur_update.reverse():
                cur_user = item.user
                m = Image.objects.filter(user = cur_user)
                if m:
                    cur_image = m[0]
                else:
                    cur_image = ''
                cur_dict = {
                    'user':cur_user.username,
                    'link':str(cur_user.slug),
                    'img':cur_image,
                    'text':item.message,
                    'time':when(item.event_date),
                    }
                res_list.append(cur_dict)
                print 2
            return direct_to_template(request, 'update_list1.html',{'res_list':res_list})
        else:
            print 1
            cur_message = Message.objects.filter(recipient = request.user)[cur_items:]
            print 1
            res_list = []
            for item in cur_message:
                cur_username = item.sender.username
                cur_profile = Profile.objects.filter(username = item.sender.username)[0]
                m = Image.objects.filter(user = cur_profile)
                if m:
                    cur_image = m[0]
                else:
                    cur_image = ''
                cur_dict = {
                    'sender':cur_username,
                    'body':item.body,
                    'time':when(item.sent_at),
                    'user_info':get_inf(cur_username),
                    'img':cur_image,
                    'link':str(cur_profile.slug),
                    'message_id':item.subject,
                    }
                res_list.append(cur_dict)

            return direct_to_template(request, 'message_list.html',{'res_list':res_list})

    else:
        return HttpResponse('ok')


def match_process(request):
    requested_user = request.GET['liked'].replace("+","")
    print requested_user
    instance = Profile.objects.get(username = requested_user)
    f = Like.objects.create(user = request.user, like = instance)
    if instance.gender == 1:
        w = 'her'
    else:
        w = 'him'
    return HttpResponse(simplejson.dumps({'gender':w}))



