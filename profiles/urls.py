from django.conf.urls.defaults import patterns, include, url
from .ajax_submit_form import submit_form
from .forms import ProfileSettingsForm, ProfileForm
from .views import save_profile_settings, save_profile


urlpatterns = patterns('profiles.views',
    url(r'^edit/$', 'edit_profile', name='edit_profile'),
    url(r'^view/(?P<slug>\w{32})/', 'view_profile', name='view_profile'),

    url(r'^upload_photo/$', 'upload_photo', name='upload_photo'),
    url(r'^send_message/$', 'send_message'),
    # AJAX URLS
    url(r'^save_settings/$', submit_form,
        {'form_class': ProfileSettingsForm,
        'callback': lambda request, *args, **kwargs: { },
        'complete_func': save_profile_settings,
        }, name='save_settings'),
    url(r'^save_profile/$', submit_form,
        {'form_class': ProfileForm,
         'callback': lambda request, *args, **kwargs: { },
        'complete_func': save_profile,
        }, name='save_profile'),

    url(r'change_location/', 'change_location', name='change_location'),
    url(r'^change_safe_mode/$', 'change_safe_mode', name='change_safe_mode'),
    url(r'^images/set_primary/(?P<slug>\w{32})/?', 'set_primary_avatar', name='set_primary_avatar'),
    url(r'^update/delete/(?P<slug>\w{32})/?', 'delete_update', name='delete_update'),
    url(r'^notifications/', include('notification.urls')),
)
