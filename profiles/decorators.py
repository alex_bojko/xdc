from django.http import (HttpResponseRedirect, HttpRequest, Http404,
                         HttpResponse)
import types
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt, csrf_protect


def only_POST(view, redirect_url='', function=None, **extra_context):
    def wrap(*args, **kwargs):
        request = None
        kwargs.update(extra_context)
        if isinstance(view, types.MethodType):
            # if view is a class method so first argument always should be
            # `self`, second request, then *args, **kwargs
            request = args[1]
        else:
            # if wrapped function is just a function so first argument always
            # should be `request`.
            request = args[0]
        if isinstance(request, HttpRequest) and request.method == 'POST':
            return view(*args, **kwargs)
        if redirect_url:
            if redirect_url.startswith('/'):
                return HttpResponseRedirect(redirect_url)
            else:
                return HttpResponseRedirect(reverse(redirect_url))
        if function and callable(function):
            return function(*args, **kwargs)
        raise Http404()
    return wrap

@csrf_exempt
def need_ajax(view):
    def wrap(request, *args, **kwargs):
        if request.is_ajax():
            resp = view(request, *args, **kwargs)
            return HttpResponse(resp)
        return Http404("This page can not be viewed")
    return wrap

def change_template(method_to_template_map):
    def wrapper(func):
        def view_wrap(request, *args, **kwargs):
            res = func(request, *args, **kwargs)
        return view_wrap
    return wrapper


