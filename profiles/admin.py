from django.contrib import admin
from .models import Profile, Image, Update
from sorl.thumbnail.admin import AdminImageMixin
from sorl.thumbnail import default

ADMIN_THUMBS_SIZE = "200x200"

class ImageAdmin(AdminImageMixin, admin.ModelAdmin):
    def approve(ma, request, queryset):
        queryset.update(need_moderation=False)
    def approve_not_safe(ma, request, queryset):
        queryset.update(need_moderation=False, is_safe=False)
    def approve_safe(ma, request, queryset):
        queryset.update(need_moderation=False, is_safe=True)

    actions = [approve, approve_not_safe, approve_safe]

    def image_display(self, obj):
        thumb = default.backend.get_thumbnail(obj.src.file, ADMIN_THUMBS_SIZE)
        return '<img src="%s" width="%s" height="%s" />' % (thumb.url, thumb.width, thumb.height)
    image_display.allow_tags = True

    list_display = ('image_display', 'user', )


class UpdateAdmin(admin.ModelAdmin):
    list_display = ('get_event_type_display', 'message', 'user', 'slug')

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('username', 'slug', 'birth_date', 'location', 'gender',
                    'looking_for', 'last_active')

admin.site.register(Image, ImageAdmin)
admin.site.register(Profile,  ProfileAdmin)
admin.site.register(Update, UpdateAdmin)

