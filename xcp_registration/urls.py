from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from profiles.decorators import only_POST
from xcp_registration.views import register

urlpatterns = patterns('xcp_registration.views',
    url(r'^save/?$', only_POST(register, redirect_url="/"),
        name='registration'),
    url(r'^step2/?$', 'registration_step2',
        name='registration_step2'),
    url(r'^step3/?$', 'registration_step3',
        name='registration_step3'),
)

