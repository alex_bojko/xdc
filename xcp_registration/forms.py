import datetime
from django import forms
from django.conf import settings
from django.forms.extras import widgets
from django.contrib.auth.models import User
from captcha.fields import CaptchaField
from sorl.thumbnail import get_thumbnail
from django.forms.widgets import Widget, Select
from django.utils.dates import MONTHS, MONTHS_3
from django.utils.safestring import mark_safe

from profiles.models import Profile, Image
from locations.models import City
import autocomplete_light
from .widgets import SelectDateWidget
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm

autocomplete_light.autodiscover()
now = datetime.datetime.now().year
YEARS_RANGE = range(now-18, now-93, -1)


class RegistrationFormStep1(forms.ModelForm):
    password = forms.CharField(label='Password', max_length=255,
                               widget=forms.PasswordInput())
    birth_date = forms.DateField(
        widget=SelectDateWidget(years=YEARS_RANGE),
        input_formats=forms.fields.DEFAULT_DATE_INPUT_FORMATS+('%m-%d-%Y',),
        required=True,)
    
    tos = forms.BooleanField(label="I agree with the Terms & Conditions",
                             widget=forms.CheckboxInput(attrs={"class": "agree"}),
                             required=True,)
    class Meta:
        model = Profile
        fields = ('email', 'password', 'birth_date')

    def clean_email(self):
        mail = self.cleaned_data.get('email', None)
        if mail:
            if User.objects.filter(email=mail):
                raise forms.ValidationError("User with the same name already exists")
            else:
                return mail
        raise forms.ValidationError("This field is required.")

    def clean_tos(self):
        if not self.cleaned_data.get('tos'):
            raise forms.ValidationError('You need to be agree with terms & Conditions')
        return self.cleaned_data['tos']

    def clean_password(self):
        if not self.cleaned_data.get('password', None):
            raise forms.ValidationError('This field is required.')
        return self.cleaned_data['password']

    def save(self, *args, **required_fields):
        data = self.cleaned_data.copy()
        # create new user
        email, password = data.pop('email'), data.pop('password')
        data.pop('tos')
        required_fields.update(data)
        return self.Meta.model.objects.create_user(email, password, **required_fields)


class RegistrationFormStep2(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('username', 'gender', 'looking_for', 'location')

    location = forms.ModelChoiceField(queryset=City.objects.all(),
                                      widget=autocomplete_light.ChoiceWidget("CityAutocomplete"))
    def __init__(self, user, *args, **kwargs):
        super(RegistrationFormStep2, self).__init__(*args, instance=user, **kwargs)
        self.fields['username'].initial = user.username
        self.fields['gender'].initial = 0
        self.fields['looking_for'].initial = 1
        self.user = user

    captcha = CaptchaField()

    def clean_location(self):
        return self.cleaned_data['location']


class RegistrationFormStep3(forms.ModelForm):
    about_me = forms.CharField(required=True, widget=forms.Textarea())
    class Meta:
        model = Profile
        fields = ('about_me',)

# class LogInForm(AuthenticationForm):

#     def clean(self):
#         username = self.cleaned_data.get('username')
#         password = self.cleaned_data.get('password')

#         if username and password:
#             self.user_cache = authenticate(username=username, password=password)
#             if self.user_cache is None:
#                 self.user_cache = Profile
#                 raise forms.ValidationError(_("Please enter a correct username and password. Note that both fields are case-sensitive."))
#             elif not self.user_cache.is_active:
#                 raise forms.ValidationError(_("This account is inactive."))
#         self.check_for_test_cookie()
#         return self.cleaned_data
