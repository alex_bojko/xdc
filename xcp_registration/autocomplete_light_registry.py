import autocomplete_light

from locations.models import City as City
from cities_light.contrib.autocompletes import CityAutocomplete

autocomplete_light.register(City, CityAutocomplete)

#from cities_light.contrib.autocompletes import CityAutocomplete
#class CityAutocomplete(autocomplete_light.AutocompleteModelBase):
    #search_fields = ('name', 'name_std',)
    #autocomplete_js_attributes={'placeholder': 'City name ..'}

#autocomplete_light.register(
    #City,
    #search_fields=('name', 'name_std'),
    #autocomplete_js_attributes={'placeholder': 'City name..'}
#)

