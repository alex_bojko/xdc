from django.views.generic.simple import direct_to_template
from django.contrib.auth import login, logout
from django.shortcuts import get_object_or_404
from django.http import (HttpResponse, HttpResponseRedirect,
                         HttpResponseNotFound)
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from django.contrib.auth import authenticate
from django.conf import settings

from .forms import (RegistrationFormStep1,RegistrationFormStep2,
                    RegistrationFormStep3,)
from profiles.models import DEFAULT_ABOUT_ME, Profile
from profiles.forms import PhotoForm
from locations.models import City
from profiles.ajax_submit_form import submit_form


def register(request):
    if request.method == 'POST':
        reg_form = RegistrationFormStep1(request.POST)
        if reg_form.is_valid():
            # authenticate() always has to be called before login(), and
            # will return the user we just created.
            loc = None
            try:
                loc = City.objects.get(name=request.META['GEOIP_CITY'])
            except City.MultipleObjectsReturned:
                loc = City.objects.filter(name=request.META['GEOIP_CITY'])[0]
            except City.DoesNotExist:
                try:
                    loc = City.objects.get(latitude=request.META['GEOIP_LATITUDE'],
                                           longitude=request.META['GEOIP_LONGTITUDE'])
                except City.DoesNotExist:
                    pass
            new_user = reg_form.save(location=loc,
                                     about_me=DEFAULT_ABOUT_ME)
            new_user = authenticate(username=new_user.username,
                                    password=reg_form.cleaned_data['password'])
            login(request, new_user)
            request.session['registration_step'] = 1
            return redirect('index')

     #   if notification:
         #   # send notification
         #   try:
        #        notification.send([profile], "profile_created", {"user": profile})
         #   except Exception as e:
         #       pass

        return direct_to_template(request, 'profiles/index.html',
                              {"reg_form": reg_form})
    return redirect('index')

@login_required
def registration_step3(request):
    if request.method == "POST":
        # save step 2 and redirect to step 3
        u_form = RegistrationFormStep3(request.POST, instance=request.user)
        img_form = PhotoForm(request.POST, request.FILES)
        #all_ok = False
        if u_form.is_valid():
            u = u_form.save()
        else:
            return direct_to_template(
                request, 'profiles/index.html',
                {'reg_form3': u_form,
                 'photo_form': img_form}
            )
        if img_form.is_valid():
            im = img_form.save(commit=False)
            im.user = request.user
            im.is_avatar = True
            im.save()
            #all_ok = True
        #if all_ok:
        del request.session['registration_step']
        return redirect('index')
            #return redirect('index')

@login_required
def registration_step2(request, *args, **kwargs):
    if request.method == "POST":
        print request.POST
        # save step 2 and redirect to step 3
        form = RegistrationFormStep2(request.user, request.POST)
        if form.is_valid():
            u = form.save(commit=False)
            u.save()
            request.session['registration_step'] = 2
            return redirect('index')
        return direct_to_template(request, 'profiles/index.html', {'reg_form2': form})
    return redirect('index')

def get_sign_up(request):
    return direct_to_template(request, 'sign_up.html', {})


def ajax_login(request, result_data, kwargs_form_data, *args, **kwargs):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)

def sign_out(request):
    logout(request)
    return redirect('index')


