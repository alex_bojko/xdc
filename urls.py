from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from xcp_registration.views import register
from xcp_registration.forms import RegistrationFormStep1  #, AuthForm
import autocomplete_light
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import login
from profiles.ajax_submit_form import submit_form
from xcp_registration.views import ajax_login
import django


admin.autodiscover()
# enable autocomplete
autocomplete_light.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'profiles.views.index', name='index'),
    #url(r'^saving_message/', 'profiles.views.save_message'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^profile/', include('profiles.urls')),
    url(r'^visits/', include('user_statistic.urls')),
    url(r'^search/', 'profiles.views.search', name='search'),
    url(r'^helpdata/', 'profiles.views.helpdata', name = 'helpdata' ),
    url(r'^subscribe_user/', 'subscribe.views.subscribe_user', name = 'subscribe_user'),
    url(r'^subscribe/', 'subscribe.views.subscribe', name = 'subscribe'),
    url(r'^datingCenter/', 'profiles.views.datingcenter', name = 'datingcenter'),
    url(r'^getUpdateData/', 'profiles.views.getUpdateData', name = 'getUpdateData'),
    url(r'^getMessageData/', 'profiles.views.getMessageData', name = 'getMessageData'),
    url(r'^getviewed/','user_statistic.views.get_viewed_widget', name = 'viewed_widget'),
    url(r'^getlikes/','user_statistic.views.get_likes_widget', name = 'likes_widget'),
    url(r'^getonline','user_statistic.views.get_online_widget', name = 'online_widget'),
    url(r'^postupdate','profiles.views.post_update', name = 'postupdate'),
    url(r'^check_payment','subscribe.views.check_payment', name = 'check_payment'),
    url(r'^getlast','user_statistic.views.get_registered_widget', name = 'get_last'),
    url(r'^delete_message/(?P<s_id>\w{32})/?', 'profiles.views.delete_message', name = 'delete_message'),
    url(r'^set_img/', 'profiles.views.set_img', name = 'set_img'),
    url(r'^del_img/', 'profiles.views.del_img', name = 'del_img'),
    url(r'^get_more/', 'profiles.views.get_more', name = 'get_more'),
    url(r'^matchProcess', 'profiles.views.match_process', name = 'match_process'),
    url(r'^get_sign_up', 'xcp_registration.views.get_sign_up', name = 'get_sign_up'),


    #url(r'^auth/login/$', submit_form,
        #{'form_class': AuthenticationForm,
         #'callback': lambda request, *args, **kwargs: { },
        #'complete_func': login,
        #}, name='auth_login'),

    url(r'^auth/login/$', submit_form,
        {'complete_func':ajax_login,
            'form_class':AuthenticationForm,
            'callback':lambda *args, **kwargs: { },
            }, name='auth_login'),
    url(r'^auth/logout','django.contrib.auth.views.logout', name='logout'),
    url(r'^auth/ajax_logout/?$', 'xcp_registration.views.sign_out',
        name='ajax_logout'),
    url(r'^auth/ajax_password_reset/', submit_form,
        {'complete_func': django.contrib.auth.views.password_reset,
         'form_class': django.contrib.auth.forms.PasswordResetForm,
         'callback': lambda *a, **k: {},
        }, name="password_reset"),
    url(r'^auth/', include('django.contrib.auth.urls')),

    url(r'^registration/', include('xcp_registration.urls')),

    #url(r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),
    url(r'^captcha/', include('captcha.urls')),

    # cities auto complete
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^messages/', include('messages.urls')),


)
urlpatterns += staticfiles_urlpatterns()



if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        url(r'^site_media/media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
