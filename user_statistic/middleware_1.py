import datetime
from profiles.models import Profile

class ActivityMiddleware(object):
    def process_request(self, request):
        if request.user.is_authenticated():
            last_activity = datetime.datetime.now()
            prof = request.user
            prof.last_active = last_activity
            prof.save()

