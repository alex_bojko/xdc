from django.conf.urls.defaults import patterns, include, url



urlpatterns = patterns('user_statistic.views',
    url(r'^viewed/all/$', 'get_viewed_all', name='get_viewed_all'),
    url(r'^viewed/widget/$', 'get_viewed_widget', name='get_viewed_widget'),
    url(r'^likes/all/$', 'get_likes_all', name='get_likes_all'),
    url(r'^likes/widget/$', 'get_likes_widget', name='get_likes_widget'),
    url(r'^online/all', 'get_online_all', name = 'get_online_all'),
    url(r'^online/widget', 'get_online_widget', name = 'get_online_widget'),
    url(r'^registered/all', 'get_registered_all', name = 'get_registered_all'),
    url(r'^registered/widget', 'get_registered_widget', name = 'get_registered_widget' ),
)
