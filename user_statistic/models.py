from django.db import models
from profiles.models import Profile

class LikeManager(models.Manager):
    def user_like(self, user):
        return self.get_query_set().filter(user=user)

    def like_user(self, user):
        return self.get_query_set().filter(like=user)




class Like(models.Model):
    user = models.ForeignKey(Profile, related_name='like')
    like = models.ForeignKey(Profile, related_name='like_user')

    creation_date = models.DateTimeField(auto_now_add=True, auto_now=True)
    is_new = models.BooleanField(default=True)

    objects = LikeManager()

    class Meta:
        unique_together = (
            ('user', 'like'),
        )
        ordering = ('-creation_date', '-is_new')

class ViewedManager(models.Manager):
    def user_viewed(self, user):
        return self.get_query_set().filter(user=user)

    def viewed_user(self, user):
        return self.get_query_set().filter(viewed=user)


class Viewed(models.Model):
    user = models.ForeignKey(Profile, related_name='+')
    viewed = models.ForeignKey(Profile, related_name='+')

    creation_date = models.DateTimeField(auto_now_add=True, auto_now=True)
    is_new = models.BooleanField(default=True)

    objects = ViewedManager()

    class Meta:
        unique_together = (
            ('user', 'viewed'),
        )
        ordering = ('-creation_date', '-is_new')
