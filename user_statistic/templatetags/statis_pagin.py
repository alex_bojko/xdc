from django import template

register = template.Library()

@register.inclusion_tag('pagination.html')
def set_pagin(pages, num):
    if len(pages) <= num or int(pages.current().label) < num:
        return {'pg': list(pages)[:num], 'pages': pages}
    else:
        pg =[]
        for i, el in enumerate(pages):
            if el.is_current:
                pg = list(pages)[i-(num/2):i] + list(pages)[i:i+(num/2+1)]
        return {'pg': pg, 'pages':pages}



    #return CurrentTimeNode(format_string[1:-1])
