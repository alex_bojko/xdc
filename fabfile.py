from fabric.api import run
from fabric.api import (env, sudo, run, local, cd)
from fabric.operations import put, get

env.hosts = ['ubuntu@xdating.im',]
ROOT_PATH = "/home/ubuntu/django/test/xdc/"



def deploy():
    local("git push")
    with cd(ROOT_PATH):
        sudo("pip install -r pipreq.txt")
        run("git pull")
        #run("rm local_settings.*")
        run("ps uax | grep python")
        run("./server.sh restart")
        run("ps uax | grep python")


def get_settings():
    with cd(ROOT_PATH):
        get("local_settings.py")

def test():
    cd(ROOT_PATH)

