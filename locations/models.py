from django.db import models, connection
from cities_light.models import City
from django.conf import settings
from profiles.models import Profile


'''
Code is never used in projec
'''
class SearchManager(models.Manager):
    def get_all_in_radius(self, lat, lng, distance=0):
        try:
            cursor = connection.cursor()
            cursor.execute(
                """SELECT id FROM %s
                WHERE DISTANCE(latitude, longitude, %s, %s) < %f;""" % (
                    self.models._meta.db_table,
                    lat, lng, getattr(settings, "LOCATION_RADIUS", float(distance))
                ))
            return [i[0] for i in cursor.fetchall()]
        except Exception as e:
            print e
            return list()



class LocationSearchModel(models.Model):
    search = SearchManager()

    class Meta:
        abstract = True

class Location(City, LocationSearchModel):
    class Meta:
        proxy = True
'''
End of never used code
'''

def get_all_in_radius(lat, lng, distance=0):
    try:
        cursor = connection.cursor()
        cursor.execute(
            """SELECT id FROM %s
            WHERE DISTANCE(latitude, longitude, %s, %s) < %f;""" % (
                City._meta.db_table,
                lat, lng, getattr(settings, "LOCATION_RADIUS", float(distance))
            ))
        return [i[0] for i in cursor.fetchall()]
    except Exception as e:
        print e
        return list()

def filter_users(lat=0, lng=0, city=None):
    if city and isinstance(city, City):
        lat, lng = city.latitude ,city.longitude
    cities = get_all_in_radius(lat=lat, lng=lng)
    return Profile.objects.filter(location__id__in=cities)


