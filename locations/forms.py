from django import forms
from django.db.models import Count
from django.conf import settings
from cities_light.models import City
import autocomplete_light
from profiles.models import Profile
from locations.models import filter_users
import datetime
import logging
logger = logging.getLogger(__name__)

class XCityForm(forms.ModelForm):
    location = forms.ModelChoiceField(
        queryset=City.objects.all(),
        widget=autocomplete_light.ChoiceWidget("CityAutocomplete")
    )
    class Meta:
        model = City


class SearchForm(forms.Form):
    def __init__(self, *args, **kwargs):
        """Reassign initial values of fields gender and looking_for according to users gender"""
        user = kwargs.pop('user', None)
        if user and isinstance(user, Profile):
         #   user = kwargs.pop('user')
            self.base_fields['looking_for'].initial = user.looking_for
            self.base_fields['gender'].initial = user.gender
        super(SearchForm, self).__init__(*args, **kwargs)

    #country = forms.ModelChoiceField(queryset=Country.objects.all())
    city = forms.ModelChoiceField(queryset=City.objects.all(),
                                  widget=autocomplete_light.ChoiceWidget("CityAutocomplete"))
    age_min = forms.ChoiceField(choices=[(0,"Any")]+[(i, i) for i in xrange(18, 100)])
    age_max = forms.ChoiceField(choices=[(i, i) for i in xrange(100, 18, -1)]+[(0, "Any")])
    is_online = forms.BooleanField(label="Online now", widget=forms.CheckboxInput(), required=False)
    gender = forms.ChoiceField(choices=Profile.GENDER_CHOICES)
    looking_for = forms.ChoiceField(choices=Profile.GENDER_CHOICES)
    with_photos = forms.BooleanField(required=False)

    def do_search(self, for_user=None):
        logger.info("do_search")
        cd = getattr(self, 'cleaned_data', None)

        if cd:
            print "Cleaned Data is here"
            city = cd['city']
            age_min = int(cd['age_min'])
            age_max = int(cd['age_max'])
            is_online = cd['is_online']
            with_photos = cd['with_photos']
            looking_for = cd['looking_for']
            gender = cd ['gender']
        else:
            print "Search by default params"
            city = for_user.location
            age_min = self.fields['age_min'].choices[0][0]
            age_max = self.fields['age_max'].choices[0][0]
            is_online = False
            with_photos = False
            looking_for = for_user.looking_for
            gender = for_user.gender

        print "\n\n", cd, "\n\n"
        now = datetime.datetime.now()
        # mixed up dates to more correct SQL
        # now max_d means  maxumum date of birth and
        # min_d means min dob
        users = filter_users(city=city).exclude(id=for_user.id)

        logger.info(users)

        users = users.filter(gender=looking_for)

        logger.info("gender = "+str(gender))
        logger.debug(users)

        if age_min:
            max_d = now.replace(year=now.year-age_min)
            users = users.filter(birth_date__lte=max_d)
            logger.info("birt_dat__lte = "+str(max_d))
            logger.info(users)
        if age_max:
            min_d = now.replace(year=now.year-age_max)
            users = users.filter(birth_date__gte=min_d)
            logger.info("birth_date__gte = "+str(min_d))
            logger.info(users)
        if is_online:
            online_time = getattr(settings, "DEFAULT_ONLINE_TIME_SEC", 50000*60)
            onl_d = now-datetime.timedelta(seconds=online_time)
            users = users.filter(last_active__gte=onl_d)
            logger.info("is_online last_login__gte "+str(onl_d))
            logger.info(users)

        if with_photos:
            users = users.annotate(images_count=Count('image')).filter(images_count__gt=0)
        return users





