from django.conf.urls.defaults import patterns, include, url
from .ajax_submit_form import submit_form
from .forms import ProfileSettingsForm, ProfileForm
from .views import save_profile_settings, save_profile


urlpatterns = patterns('locations.views',
)
