from django.contrib import admin
from cities_light.models import City

admin.site.unregister(City)
admin.site.register(City)
