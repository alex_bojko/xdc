import re
import platform
from .settings import *
import os

file_re = r'^\w*_settings.py$'


#for  dp, dn, df in os.walk("./settings"):
#    for f in df:
#        if re.match(file_re, f):
#            print "GOOG "+f
#            m = __import__(f.split(".")[0])
#            for k in dir(m):
#                __all__.append(k)


def match(name):
    return True if re.match(s_map[name], uname) else False

# servers name and username on this server
s_map = {
    'dev': 'alex-PORTEGE',
    'production': 'ip-10-110-49-125',
    'office': 'exadel'
}
for k, v in s_map.items():
    s_map[k] = "^"+v+"$"

uname = platform.node()

if match('dev'):
    print "DEV SETTINGS"
    try:
        from local_settings import *
    except ImportError:
        pass
elif match('production'):
    print "PRODUCTION SETTINGS"
    try:
        from production_settings import *
    except ImportError:
        pass
elif match('office'):
    print "OFFICE SETTINGS"
    try:
        from exadel_settings import *
    except ImportError:
        pass
else:
    print "NO ONE SETTING FILE USED"
