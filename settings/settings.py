# Django settings for xcp project.
import os

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

build_path = lambda *a: (
     os.path.realpath(os.path.join(os.path.dirname(__file__),  *a))
)


ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.mysql',
       #'ENGINE': 'django.db.backends.mysql',
        'NAME': 'xcp',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
   }
}
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1
SITE_NAME = 'http://xdating.im'
# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
USE_TZ = True
# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = build_path('..', 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/site_media/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = build_path('..', 'all_static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(ROOT_DIR, 'static'),
    #build_path('static'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 't4wbv2#0hwylys3%aoyni-qqe8aua)7gvhrmfqeba2cgh&=%%z'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (

    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'user_statistic.middleware_1.ActivityMiddleware',

)

ROOT_URLCONF = 'xdc.urls'

TEMPLATE_DIRS = (
    os.path.join(ROOT_DIR, 'templates'),
    #build_path('templates')
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

AUTHENTICATION_BACKENDS = (
    'profiles.auth_backends.CustomUserModelBackend',
    #'django.contrib.auth.backends.ModelBackend',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "profiles.context_processors.settings",
    "xcp_registration.context_processors.reg_auth_forms",

#     "session_messages.context_processors.session_messages",
)


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.flatpages',

    'cities_light',
    'notification',
    'sorl.thumbnail',
    'south',


    'profiles',
    'xcp_registration',
    'locations',
    'user_statistic',

    'captcha',
    'autocomplete_light',
    'endless_pagination',
    'messages',
    'helpdata',
    'subscribe',
)


# captcha settings
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.random_char_challenge'
CAPTCHA_LENGTH = 3
CAPTCHA_FOREGROUND_COLOR = "#FF0000"


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': build_path(ROOT_DIR, "logs", "logfile"),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers':['console'],
            'propagate': True,
            'level':'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        '': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
    }
}
# endless-pagination settings
ENDLESS_PAGINATION_PREVIOUS_LABEL = u"Prev"
ENDLESS_PAGINATION_NEXT_LABEL = u"Next"

# User statistic
ENDLESS_PAGINATION_PREVIOUS_LABEL = u"Prev"
ENDLESS_PAGINATION_NEXT_LABEL = u"Next"
COUNT_VISIBLE_ITEMS_BLOCK = 8
COUNT_VISIBLE_ITEMS_PAGE = 12
COUNT_VISIBLE_PAGINATION_BLOCK = 5
COUNT_VISIBLE_PAGINATION_PAGE = 10

#location radius (km) only float
LOCATION_RADIUS = 1000.0

CUSTOM_USER_MODEL = 'profiles.Profile'
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'

DATE_FORMAT = "%m-%d-%Y"
DATE_INPUT_FORMATS = ('%m-%d-%Y',)

CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.random_char_challenge'
CAPTCHA_NOISE_FUNCTIONS = ()
try:
    #from production_settings import *
    from local_settings import *
except ImportError:
    pass

'''try:
    from production_settings import *
    #from local_settings import *
except ImportError:
    pass'''


AUTH_PROFILE_MODULE = 'profiles.Profile'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'onePieceOfEmail@gmail.com'
EMAIL_HOST_PASSWORD = 'stronpassword'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
