#!/bin/bash

case "$1" in
"start")
#есть 2 варианта запуска сервера, по сети и через unix сокеты
# последний выигрывает по производительности
python ./manage.py runfcgi method=prefork host=127.0.0.1 port=8080 pidfile=/tmp/sitename.pid
#python ./manage.py runfcgi method=prefork socket=/tmp/xdat.socket pidfile=/tmp/sitename.pid
# не забываем, про то, что сокет у нас мог прочитать фронт-энд
# а nginx  у меня работает с правами www-data
#chown www-data:www-data /tmp/sitename.sock
;;
"stop")
kill -9 `cat /tmp/sitename.pid`
;;
"restart")
$0 stop
sleep 1
$0 start
;;
*) echo "Usage: ./server.sh {start|stop|restart}";;
esac
